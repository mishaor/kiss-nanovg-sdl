### kiss-nanovg-sdl

a library for creating gui applications with a nice "svg" look courtesy of nanovg, primarily targeted at android devices. The base for this library is kiss-sdl, heavily modified in some places in order to work nicely with nanovg and to better suit the purpose of this library.

Some code is commented out, which means it has not been replaced yet (like scrollbars), but the replacement would be non-trivial (like smooth scrolling akin to what android already does in browsers / java applications).

It is my hope, (not really realistic, I know) that this library would get extended enough to make making android applications in C something even people not as dedicated to it as I am can consider.

### screenshot

![example app screenshot](screenshot.png "example app")

checkbuttons also look really nice, but I don't have a screenshot with those  
[Hacker's keyboard](https://github.com/klausw/hackerskeyboard) not included

### contributing

If you want to contribute, that is definitely appreciated, ideally create an issue first and make sure to find a way to contact me if I don't respond for a suspiciously long time. I'm not sure I trust gitlab notifications just yet.

Right now, it's in a state where if you have a relatively simple project, and you believe in using C on android, you can just add a few widgets and you should be good to go. If you're looking for something with all the widgets already there, well that's what this can hopefully become if enough people get involved.

If you just feel like making some widgets, but have no need for any concrete ones, I suggest taking inspiration from normal java android stuff.

### license

This library is under LGPLv3, with both nanovg and kiss containing additional restrictions because of their original zlib license. If you want to use just kiss or nanovg, you can visit their repositories for the original versions that only contain zlib license covered code. As already stated, kiss has been modified substantially with LGPLv3 code.

I believe LGPL is a fair license, because as long as you link it dynamically (I can make that easier than it's right now if you request), it doesn't affect the license of your applications code (the one you may wish to be of proprietary nature). And if you make modifications to this library, you're not going to mind sharing that modifications nearly as much, if at all.

Of course I'm not a lawyer, and if you believe your use case isn't possible under these terms, please contact me so I can change your mind or add it here as something to consider.
